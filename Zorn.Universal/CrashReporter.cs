﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

using Windows.Web.Http;

using Zorn.Universal.Net;

namespace Zorn.Universal
{
    public class CrashReporter
    {
        public string AppName = string.Empty;
        public string AppVersion = string.Empty;
        public string ReportEndpoint;

        private List<string> _log = new List<string>();

        private PersistentData<CrashReport> LastCrashReport = new PersistentData<CrashReport>("CrashReport", null);

        public CrashReporter(string appName, string appVersion, string reportEndpoint)
        {
            AppName = appName;
            AppVersion = appVersion;
            ReportEndpoint = reportEndpoint;
        }

        public void Log(string message)
        {
            _log.Add(message);
            System.Diagnostics.Debug.WriteLine(message);
        }

        public void ClearLog()
        {
            _log.Clear();
        }

        public void ReportException(Exception ex, string info)
        {
            if (!string.IsNullOrWhiteSpace(info))
            {
                _log.Add(info);
            }

            LastCrashReport.Value = new CrashReport(ex, string.Join("\n", _log));
        }

        public CrashReport CheckForPreviousCrash()
        {
            var report = LastCrashReport.Value;

            if (report != null)
            {
                LastCrashReport.Value = null;

                var payload = report.Payload;
                payload.Add("app_name", AppName);
                payload.Add("app_version", AppVersion);

                Task.Run(async () =>
                {
                    var response = await NetUtil.GetResponseAsync(
                        new HttpClient(),
                        NetUtil.CreateUrl(ReportEndpoint, payload));
                });
            }

            return report;
        }

        [DataContract]
        public class CrashReport
        {
            [DataMember]
            public Dictionary<string, string> Payload = new Dictionary<string, string>();

            public CrashReport() { }

            public CrashReport(Exception exception, string info)
            {
                Payload = ToPayload(exception, info);
            }

            private Dictionary<string, string> ToPayload(Exception exception, string info)
            {
                var innerException = exception.InnerException ?? new Exception();
                return new Dictionary<string, string>()
                    {
                        {"exception_message", exception.Message},
                        {"exception_stacktrace", exception.StackTrace},
                        {"exception_data", Convert.ToString(exception.Data)},
                        {"exception_helplink", exception.HelpLink},
                        {"exception_hresult", Convert.ToString(exception.HResult)},
                        {"exception_source", exception.Source},
                        {"innerexception_message", innerException.Message},
                        {"innerexception_stacktrace", innerException.StackTrace},
                        {"info", info}
                    };
            }
        }

    }
}
