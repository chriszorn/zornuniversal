﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json.Linq;

namespace Zorn.Universal
{
    public static class JsonExtensions
    {
        public static string MaybeGetValueString(this JToken obj, string key)
        {
            if (obj == null || obj.Type == JTokenType.Null) { return default(string); }

            var value = obj[key];

            return value != null
                ? value.ToString()
                : default(string);
        }

        public static int MaybeGetValueInt(this JToken obj, string key)
        {
            if (obj == null || obj.Type == JTokenType.Null) { return default(int); }

            var value = obj[key];

            if (value != null)
            {
                try
                {
                    return Convert.ToInt32(value.ToString());
                }
                catch { }
            }

            return default(int);
        }

        public static bool MaybeGetValueBool(this JToken obj, string key)
        {
            if (obj == null || obj.Type == JTokenType.Null) { return default(bool); }

            var value = obj[key];

            if (value != null)
            {
                try
                {
                    return value.ToString().ToLower() == "true";
                }
                catch { }
            }

            return default(bool);
        }
    }
}
