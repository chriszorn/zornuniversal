﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.Web.Http;
using Windows.Web.Http.Headers;
using Windows.UI.Popups;

using Zorn.Universal.Net;

namespace Zorn.Universal
{
    public class ApplicationUtil
    {
        public static async void CheckForAppUpdate(string appStoreUrl, string version)
        {
            var dispatcher = Windows.UI.Core.CoreWindow.GetForCurrentThread().Dispatcher;

            await Task.Run(async () =>
            {
                var httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.UserAgent.Add(new HttpProductInfoHeaderValue("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36"));
                var response = await NetUtil.GetResponseAsync(httpClient, appStoreUrl);

                if (response == null || string.IsNullOrWhiteSpace(response.Content)) { return; }

                var match = Regex.Match(response.Content, @"<span itemprop=""softwareVersion"">([^<]+)</span>");

                if (match.Groups.Count > 1)
                {
                    string appVersion = match.Groups[1].Value;

                    int[] appVersionParts = appVersion.Split(new char[] { '.' }).Select(x => Convert.ToInt32(x)).ToArray();
                    int[] versionParts = version.Split(new char[] { '.' }).Select(x => Convert.ToInt32(x)).ToArray();
                    bool existsUpdate = false;

                    for (int i = 0; i < versionParts.Length && i < appVersionParts.Length; i++)
                    {
                        if (appVersionParts[i] < versionParts[i])
                        {
                            break;
                        }
                        else if (appVersionParts[i] > versionParts[i])
                        {
                            existsUpdate = true;
                            break;
                        }
                    }

                    if (existsUpdate)
                    {
                        var messageDialog = new MessageDialog("An update is available. Click OK to download", "Update");

                        messageDialog.Commands.Add(new UICommand(
                            "ok",
                            new UICommandInvokedHandler(async (cmd) =>
                            {
                                await Windows.System.Launcher.LaunchUriAsync(new Uri(appStoreUrl, UriKind.Absolute));
                            })));

                        messageDialog.Commands.Add(new UICommand(
                            "cancel",
                            new UICommandInvokedHandler((cmd) => { })));

                        messageDialog.DefaultCommandIndex = 1;
                        messageDialog.CancelCommandIndex = 1;

                        await dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
                        {
                            try
                            {
                                await messageDialog.ShowAsync();
                            }
                            catch { }
                        });
                    }
                }
            });
        }
    }
}
