﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zorn.Universal
{
    public static class NumericUtil
    {
        public static string AbbreviateNumber(long longValue)
        {
            double value = Convert.ToDouble(longValue);
            double dividedValue;
            string thing;

            if (value > 1000000000)
            {
                dividedValue = value / 1000000000.0;
                thing = "B";
            }
            else if (value > 1000000)
            {
                dividedValue = value / 1000000.0;
                thing = "M";
            }
            else if (value > 1000)
            {
                dividedValue = value / 1000.0;
                thing = "K";
            }
            else
            {
                dividedValue = value;
                thing = string.Empty;
            }

            return string.Format("{0}{1}", dividedValue.ToString("##0.#"), thing);
        }
    }
}
