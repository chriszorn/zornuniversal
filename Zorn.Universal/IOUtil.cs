﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.Storage;
using Windows.Storage.Streams;

namespace Zorn.Universal
{
    public static class IOUtil
    {
        public static async Task<string> ReadStringFromResource(Uri uri)
        {
            if (uri == null)
            {
                throw new ArgumentNullException("uri", "uri cannot be null");
            }

            try
            {
                StorageFile storageFile = await StorageFile.GetFileFromApplicationUriAsync(uri);
                StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(uri);

                return await FileIO.ReadTextAsync(file);
            }
            catch { }

            return null;
        }

        public static async Task<string> ToStringPrimitive(this StorageFile file)
        {
            IBuffer buffer = await FileIO.ReadBufferAsync(file);
            DataReader reader = DataReader.FromBuffer(buffer);
            byte[] fileContent = new byte[reader.UnconsumedBufferLength];
            reader.ReadBytes(fileContent);
            return Encoding.UTF8.GetString(fileContent, 0, fileContent.Length);
        }

        public static async Task<byte[]> ToByteArray(this StorageFile file)
        {
            IBuffer buffer = await FileIO.ReadBufferAsync(file);
            DataReader reader = DataReader.FromBuffer(buffer);
            var data = new byte[reader.UnconsumedBufferLength];
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = reader.ReadByte();
            }

            return data;
        }
    }
}
