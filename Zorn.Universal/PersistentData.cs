﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace Zorn.Universal
{
    public class PersistentData<T>
    {
        private T _value;
        public T Value
        {
            get
            {
                if (_hasValue)
                {
                    return _value;
                }


                Deserialize();

                _hasValue = true;

                return _value;
            }
            set
            {
                _value = value;
                Serialize();
            }
        }

        public T DefaultValue;

        private bool _hasValue;
        private string _name;
        private StorageFolder localFolder;

        public PersistentData(string name, T defaultValue)
        {
            _name = name;
            DefaultValue = defaultValue;

            localFolder = ApplicationData.Current.LocalFolder;
        }

        public void Deserialize()
        {
            try
            {
                var file = localFolder.CreateFileAsync(GetFilePath(), CreationCollisionOption.OpenIfExists)
                            .AsTask().ConfigureAwait(false).GetAwaiter().GetResult();

                if (file != null)
                {
                    string xml = FileIO.ReadTextAsync(file).AsTask().ConfigureAwait(false).GetAwaiter().GetResult();
                    if (!string.IsNullOrWhiteSpace(xml))
                    {
                        _value = (T)DataSerializer.Deserialize(xml, typeof(T));
                        return;
                    }
                }
            }
            catch { }

            _value = DefaultValue;
        }

        public void Serialize()
        {
            string xml = DataSerializer.Serialize(_value);

            var file = localFolder.CreateFileAsync(GetFilePath(), CreationCollisionOption.OpenIfExists)
                .AsTask().ConfigureAwait(false).GetAwaiter().GetResult();
            FileIO.WriteTextAsync(file, xml)
                .AsTask().ConfigureAwait(false).GetAwaiter().GetResult();
        }

        private string GetFilePath()
        {
            return string.Format("PD_{0}.txt", _name);
        }
    }
}
