﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;


namespace Zorn.Universal
{
    public class GenericGroup<T> : ObservableCollection<T>
    {
        public object Key
        {
            get;
            set;
        }

        public GenericGroup()
        {
            this.Key = string.Empty;
        }

        public GenericGroup(object name, IEnumerable<T> items)
        {
            this.Key = name;

            foreach (T item in items)
                this.Add(item);
        }
    }
}
