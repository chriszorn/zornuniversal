﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.DataTransfer;

namespace Zorn.Universal
{
    public static class Share
    {
        public static void ShareUrl(string url, string title, string description)
        {
            DataTransferManager dataTransferManager = DataTransferManager.GetForCurrentView();
            dataTransferManager.DataRequested += (sender, e) => 
                {
                    DataRequest request = e.Request;
                    request.Data.Properties.Title = title;
                    request.Data.Properties.Description = description;
                    request.Data.SetWebLink(new Uri(url));                    
                };

            Windows.ApplicationModel.DataTransfer.DataTransferManager.ShowShareUI();
        }
    }
}
