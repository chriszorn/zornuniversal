﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;


namespace Zorn.Universal
{
    public static class StringUtil
    {
        public static string Base64Decode(string base64String)
        {
            if (base64String == null)
            {
                return string.Empty;
            }

            byte[] text = Convert.FromBase64String(base64String);
            return System.Text.Encoding.UTF8.GetString(text, 0, text.Length);
        }

        public static string GetHostFromUrl(string url)
        {
            string pattern = @"(?://)(?:www.)?(.+\.[^/]+)";
            var matches = Regex.Match(url, pattern);
            if(matches.Groups.Count > 0)
            {
                return matches.Groups[1].Value;
            }

            return string.Empty;
        }

        public static string GetFirstDigitsOfVersion(string version, int numDigits)
        {
            return string.Join(".", version.Split(new char[] { '.' }).Take(numDigits));
        }

        public static bool IsUrl(string text)
        {
            if(string.IsNullOrWhiteSpace(text)) { return false; }

            string pattern = @"((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)";
            return Regex.IsMatch(text, pattern);
        }
    }
}
