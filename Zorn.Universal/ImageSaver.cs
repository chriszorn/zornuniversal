﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.Storage.Provider;
using Windows.Storage.Pickers;
using Windows.ApplicationModel.Activation;
using Windows.Graphics.Imaging;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Web.Http;

namespace Zorn.Universal
{
    public class ImageSaver
    {
        private readonly IList<string> SupportedImageFileTypes = new List<string> { ".png" };

        public async Task<bool> SaveImage(string suggestedFileName, string url)
        {
            //FileSavePicker savePicker = new FileSavePicker();
            //savePicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            //savePicker.FileTypeChoices.Add("Image", SupportedImageFileTypes);
            //savePicker.SuggestedFileName = suggestedFileName  ?? "New Document";
            //savePicker.DefaultFileExtension = ".png";

            var response = await new HttpClient().GetAsync(new Uri(url, UriKind.Absolute));
            var randomAccessStream = new InMemoryRandomAccessStream();
            var outputStream = randomAccessStream.GetOutputStreamAt(0);
            await response.Content.WriteToStreamAsync(outputStream);

            var imageFile = await KnownFolders.PicturesLibrary.CreateFileAsync(suggestedFileName, CreationCollisionOption.ReplaceExisting);
            //var fs = await imageFile.OpenAsync(FileAccessMode.ReadWrite);
            //var writer = fs.GetOutputStreamAt(0);
            

            //BitmapDecoder bmpDecoder = await BitmapDecoder.CreateAsync(randomAccessStream);
            //PixelDataProvider pixelData = await bmpDecoder.GetPixelDataAsync(BitmapPixelFormat.Rgba8, BitmapAlphaMode.Straight, null, ExifOrientationMode.RespectExifOrientation, ColorManagementMode.DoNotColorManage);

            //using (var destFileStream = await imageFile.OpenAsync(FileAccessMode.ReadWrite))
            //{
            //    BitmapEncoder bmpEncoder = await BitmapEncoder.CreateAsync(BitmapEncoder.PngEncoderId, destFileStream);
            //    uint yourWidthAndOrHeight = 1024;
            //    bmpEncoder.SetPixelData(BitmapPixelFormat.Rgba8, BitmapAlphaMode.Straight, yourWidthAndOrHeight, yourWidthAndOrHeight, 300, 300, pixelData.DetachPixelData());
            //    await bmpEncoder.FlushAsync();
            //}

            
		    using (var destinationStream = await imageFile.OpenAsync(FileAccessMode.ReadWrite))
		    {
			    using (var destinationOutputStream = destinationStream.GetOutputStreamAt(0))
			    {
				    await RandomAccessStream.CopyAndCloseAsync(randomAccessStream, destinationStream);
			    }
		    }

            //StorageFile file = await savePicker.PickSaveFileAsync();
            //if (file != null)
            //{

            //    var bitmapImage = image.Source as Windows.UI.Xaml.Media.Imaging.WriteableBitmap;
            //    bitmapImage
            //    var writeableBmp = new Windows.UI.Xaml.Media.Imaging.WriteableBitmap(
                //bitmapImage.s

                //CachedFileManager.DeferUpdates(file);

                //await FileIO.WriteTextAsync(file, file.Name);

                //FileUpdateStatus status = await CachedFileManager.CompleteUpdatesAsync(file);
                //if (status == FileUpdateStatus.Complete)
                //{
                //    return true;
                //}
                //else
                //{
                //    return false;
                //}
            //}

            return true;
        }
    }
}
