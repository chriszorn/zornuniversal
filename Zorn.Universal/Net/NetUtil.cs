﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
//using System.Net;
using System.Text;
using System.Threading.Tasks;
using Windows.Web.Http;
using Windows.Web.Http.Filters;

namespace Zorn.Universal.Net
{
    public static class NetUtil
    {
        public static async Task<NetResponse> GetResponseAsync(HttpClient httpClient, string url)
        {
            HttpResponseMessage response = null;
            NetResponse ret = null;

            try
            {
                response = await httpClient.GetAsync(new Uri(url));
                ret = new NetResponse()
                {
                    Content = await response.Content.ReadAsStringAsync(),
                    Response = response,
                    StatusCode = response.StatusCode,
                };
            }
            catch (Exception)
            {
                ret = new NetResponse()
                {
                    StatusCode = HttpStatusCode.BadRequest,
                };
            }

            return ret;
        }

        public async static Task<NetResponse> PostAsync(HttpClient httpClient, string url, IHttpContent content)
        {
            HttpResponseMessage response = null;
            NetResponse ret = null;

            try
            {
                response = await httpClient.PostAsync(new Uri(url), content);
                ret = new NetResponse()
                {
                    Content = await response.Content.ReadAsStringAsync(),
                    Response = response,
                    StatusCode = response.StatusCode,
                };
            }
            catch (Exception)
            {
                ret = new NetResponse()
                {
                    StatusCode = HttpStatusCode.BadRequest,
                };
            }

            return ret;
        }

        public static async Task<NetResponse> HttpGetRequestResponse(string url)
        {
            return await GetResponseAsync(new HttpClient(), url);
        }

        public static string CreateUrl(string url, Dictionary<string, string> paramaters)
        {
            return CreateUrl(url, paramaters, false);
        }

        public static string CreateUrl(string url, Dictionary<string, string> parameters, bool forceClearCache)
        {
            if (forceClearCache)
            {
                parameters.Add("cachee", DateTime.Now.Ticks.ToString());
            }

            return string.Format(
                "{0}?{1}",
                url,
                string.Join("&", parameters.Select(x => string.Format("{0}={1}", x.Key, System.Net.WebUtility.UrlEncode(x.Value))).ToArray<string>()));
        }

        public static Dictionary<string, string> UrlEncodedParamStringToDict(string paramString)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            if (!string.IsNullOrWhiteSpace(paramString))
            {
                var paramList = paramString.Split(new char[] { '&' });
                foreach (var p in paramList)
                {
                    var kvp = p.Split(new char[] { '=' });
                    if (kvp.Count() == 2 && !result.ContainsKey(kvp.ElementAt(0)))
                    {
                        result.Add(kvp.ElementAt(0), kvp.ElementAt(1));
                    }
                }
            }

            return result;
        }

        public static void MessupCookies(Uri uri)
        {
            HttpBaseProtocolFilter filter = new HttpBaseProtocolFilter();
            var cookies = filter.CookieManager.GetCookies(uri);
            foreach (HttpCookie c in cookies)
            {
                filter.CookieManager.DeleteCookie(c);
                c.Value = "asdasdasdasdasdasdasda";
                filter.CookieManager.SetCookie(c);
            }
        }
    }
}
