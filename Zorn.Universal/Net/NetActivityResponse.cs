﻿using System;
using Windows.Web.Http;

namespace Zorn.Universal.Net
{
    public class NetActivityResponse
    {
        public string Content { get; set; }
        public object Data { get; set; }
        public string Message { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public bool Success { get; set; }
    }
}
