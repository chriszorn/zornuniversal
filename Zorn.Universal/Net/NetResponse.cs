﻿using System;
using Windows.Web.Http;

namespace Zorn.Universal.Net
{
    public class NetResponse
    {
        public HttpStatusCode StatusCode { get; set; }
        public string Content { get; set; }
        public HttpResponseMessage Response { get; set; }
    }
}
