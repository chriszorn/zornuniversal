﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace Zorn.Universal
{
    public static class VisualTreeUtil
    {
        public static T CastObjectDataContext<T>(FrameworkElement obj)
        {
            return (T)obj.DataContext;
        }

        public static T CastObjectDataContext<T>(object obj)
        {
            if (!(obj is FrameworkElement))
            {
                return default(T);
            }
            return CastObjectDataContext<T>((FrameworkElement)obj);
        }

        public static Rect GetElementRect(FrameworkElement element)
        {
            GeneralTransform buttonTransform = element.TransformToVisual(null);
            Point point = buttonTransform.TransformPoint(new Point());
            return new Rect(point, new Size(element.ActualWidth, element.ActualHeight));
        }

        public static T FindParentOfType<T>(DependencyObject childElement) where T : DependencyObject
        {
            var parents = FindParentsOfType<T>(childElement);

            return parents.Any()
                ? parents.First()
                : null;
        }

        public static List<T> FindParentsOfType<T>(DependencyObject childElement) where T : DependencyObject
        {
            List<T> parents = new List<T>();

            while (childElement != null)
            {
                childElement = ((FrameworkElement)childElement).Parent as FrameworkElement;

                if (childElement is T)
                {
                    parents.Add((T)childElement);
                }
            }

            return parents;
        }

        public static List<T> FindElementsOfType<T>(DependencyObject parentElement) where T : DependencyObject
        {
            List<T> items = new List<T>();

            var count = VisualTreeHelper.GetChildrenCount(parentElement);
            if (count == 0)
                return items;

            for (int i = 0; i < count; i++)
            {
                var child = VisualTreeHelper.GetChild(parentElement, i);

                if (child != null && child is T)
                {
                    items.Add((T)child);
                }

                var result = FindElementsOfType<T>(child);
                if (result.Count > 0)
                {
                    items.AddRange(result);
                }
            }

            return items;
        }

        //public static UIElement GetPageRoot(DependencyObject element)
        //{
        //    while (VisualTreeHelper.GetParent(element) != null)
        //    {
        //        element = VisualTreeHelper.GetParent(element);
        //    }

        //    return (UIElement)element;
        //}
    }
}
