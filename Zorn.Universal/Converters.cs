﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Windows;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace Zorn.Universal.Converters
{
    public class ConverterBase : IValueConverter
    {
        public virtual object Convert(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
        public virtual object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    public class BoolToVisibility : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, string language)
        {
            return (bool)value ? Visibility.Visible : Visibility.Collapsed;
        }
    }

    public class BoolInverseToVisibility : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, string language)
        {
            return (bool)value ? Visibility.Collapsed : Visibility.Visible;
        }
    }

    public class BoolToInverse : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, string language)
        {
            return !(bool)value;
        }
    }

    public class DoubleGreaterThanZeroToVisibility : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, string language)
        {
            double val = System.Convert.ToDouble(value);
            return val > 0 ? Visibility.Visible : Visibility.Collapsed;
        }
    }

    public class DoubleLessThanOrEqualsZeroToVisibility : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, string language)
        {
            double val = System.Convert.ToDouble(value);
            return val <= 0 ? Visibility.Visible : Visibility.Collapsed;
        }
    }

    public class IEnumerableToCount : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, string language)
        {
            var result = ((IEnumerable)value).Cast<object>().ToList();
            return result.Count();
        }
    }

    /// <summary>
    /// Returns Visible if IEnumerable is empty or null
    /// </summary>
    public class IEnumerableInverseToVisibility : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value == null) { return Visibility.Visible; }

            var result = ((IEnumerable)value).Cast<object>().ToList();
            return (result == null || result.Count == 0) ? Visibility.Visible : Visibility.Collapsed;
        }
    }


    /// <summary>
    /// Returns Visible if IEnumerable is non-empty
    /// </summary>
    public class IEnumerableToVisibility : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value == null) { return Visibility.Collapsed; }

            var result = ((IEnumerable)value).Cast<object>().ToList();
            return (result != null && result.Count() > 0) ? Visibility.Visible : Visibility.Collapsed;
        }
    }

    public class IndexToCardinal : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, string language)
        {
            return (int)value + 1;
        }
    }

    public class LongGreaterThanZeroToVisibility : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, string language)
        {
            long val = System.Convert.ToInt64(value);
            return val > 0 ? Visibility.Visible : Visibility.Collapsed;
        }
    }

    public class NumberToAbbreviatedString : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, string language)
        {
            return NumericUtil.AbbreviateNumber(System.Convert.ToInt64(value));
        }
    }

    /// <summary>
    /// E.g. 1827389 => 1,827,389
    /// </summary>
    public class ObjectToCommaSeparatedNumberString : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, string language)
        {
            return String.Format("{0:N0}", value);
        }
    }

    public class ObjectInverseToVisibility : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, string language)
        {
            return value == null ? Visibility.Visible : Visibility.Collapsed;
        }
    }

    public class ObjectToVisibility : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, string language)
        {
            return value != null ? Visibility.Visible : Visibility.Collapsed;
        }
    }

    public class StringInverseToVisibility : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, string language)
        {
            return string.IsNullOrWhiteSpace((string)value) ? Visibility.Collapsed : Visibility.Visible;
        }
    }

    public class StringToLower : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value != null)
            {
                return value.ToString().ToLowerInvariant();
            }

            return string.Empty;
        }
    }

    public class StringToUpper : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value != null)
            {
                return value.ToString().ToUpperInvariant();
            }

            return string.Empty;
        }
    }

    public class StringToVisibility : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, string language)
        {
            return !string.IsNullOrWhiteSpace((string)value) ? Visibility.Visible : Visibility.Collapsed;
        }
    }

    public class ToggleSwitchYesNo : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, string language)
        {
            return (bool)value ? "Yes" : "No";
        }
    }

    public class DateTimeToTime : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, string language)
        {
            var dateTime = (DateTime)value;
            return dateTime.ToString("h:mm tt");
        }
    }

    public class DateTimeToDate : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, string language)
        {
            var dateTime = (DateTime)value;
            return dateTime.ToString("dddd MMMM d, yyyy");
        }
    }
}
