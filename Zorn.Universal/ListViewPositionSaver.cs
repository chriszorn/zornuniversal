﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Zorn.Universal
{
    public class ListViewPositionSaver
    {
        private Dictionary<string, int> ListViewItemIndices { get; set; }

        public ListViewPositionSaver()
        {
            ListViewItemIndices = new Dictionary<string, int>();
        }

        public void ListView_Loaded(object sender, RoutedEventArgs e)
        {
            var listView = (ListViewBase)sender;
            if (ListViewItemIndices.ContainsKey(listView.Name))
            {
                var lastVisibleItemIndex = ListViewItemIndices[listView.Name];

                if (lastVisibleItemIndex >= 0 && lastVisibleItemIndex < listView.Items.Count)
                {
                    var item = listView.Items[lastVisibleItemIndex];
                    if (item != null)
                    {
                        listView.ScrollIntoView(item);
                    }
                }
            }
            else
            {
                ListViewItemIndices.Add(listView.Name, -1);
            }
        }

        public void SavePositions(IEnumerable<ListViewBase> listViews)
        {
            ListViewItemIndices.Clear();

            foreach (var listView in listViews)
            {
                int lastVisibleItemIndex = GetLastVisibleIndex(listView);

                if (lastVisibleItemIndex > -1)
                {
                    if (!ListViewItemIndices.ContainsKey(listView.Name))
                    {
                        ListViewItemIndices.Add(listView.Name, lastVisibleItemIndex);
                    }
                    else
                    {
                        ListViewItemIndices[listView.Name] = lastVisibleItemIndex;
                    }
                }
            }
        }

        private static int GetLastVisibleIndex(ListViewBase listView)
        {
            var isp = listView.ItemsPanelRoot as ItemsStackPanel;
            if (isp != null)
            {
                return isp.LastVisibleIndex;
            }

            var iwg = listView.ItemsPanelRoot as ItemsWrapGrid;
            if (iwg != null)
            {
                return iwg.LastVisibleIndex;
            }

            return -1;
        }
    }
}
